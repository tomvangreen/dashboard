Tags = new Mongo.Collection('tags');

Tag = new SimpleSchema({
	tag: {
		type: String
		, label: "Tag"
	}
	, elements: {
		type: Object
		, label: "Elements"
	}
	, count: {
		type: Number
		, label: "Element Count"
	}
	, type: {
		type: String
		, label: "Type"
	}
});
Tags.attachSchema(Tag);

TagService = {
	assigned: function(tag, item, type){
		var t = Tags.findOne({tag: tag});
		if(t){
			t.elements.push(item._id);
			Tags.update(t);
		}else{
			t = {
				tag: tag
				, elements: [item._id]
				, count: 1
				, type: type
			};
			Tags.insert(t);
		}
	}
	, unassigned: function(tag, item){

	}
};
