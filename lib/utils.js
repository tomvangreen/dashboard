Utils = {
	/**
	* Adds all elements from a source array/object into a target array
	*/
	addAll: function(elements, target){
		for(var index in elements){
			target.push(elements[index]);
		}
	}
	, sort: function(list, comparator, reverse){
		if(typeof comparator == "string"){
			comparator = Utils.createBasicComparator(comparator);
		}
		if(reverse){
			comparator = Utils.invertComparator(comparator);
		}
		return list.sort(comparator);
	}
	, invertComparator: function(comparator){
		return function(a, b){
			return comparator(a, b) * -1;
		}
	}
	, createBasicComparator: function(field){
		var comparator = function(a, b){
			var af = a[field];
			if(!af){
				af = 0;
			}
			var bf = b[field];
			if(!bf){
				bf = 0;
			}
			if(typeof af == 'string'){
						af = af.toLowerCase();
			}
			if(typeof bf == 'string'){
				bf = bf.toLowerCase();
			}
			if(af > bf){
				return 1;
			}
			else if(af < bf){
				return -1;
			}
			return 0;
		};
		return comparator;
	}
	, loadTags: function(item){
		var tags = [];
		if(item.tags){
			for(var index in item.tags){
				tags.push({tag: item.tags[index]});
			}
		}
		return tags;
	}
	, filterAnd: function(filters){
		return function(element){
			for(var index in filters){
				var filter = filters[index];
				if(!filter(element)){
					return false;
				}
			}
			return true;
		};
	}
	, filterOr: function(filters){
		return function(element){
			for(var index in filters){
				var filter = filters[index];
				if(filter(element)){
					return true;
				}
			}
			return false;
		};
	}
	, containsFilter: function(field, fragment){
		return function(element){
			var value = element[field];
			if(!value){
				return false;
			}
			if(typeof value == "string"){
				var lc = value.toLowerCase();
				return lc.indexOf(fragment) >= 0;
			}
			else if(typeof value == "object" && value.length){
				for(var index in value){
					var lc = value[index].toLowerCase();
					if(lc.indexOf(fragment) >= 0){
						return true;
					}
				}
			}
			return false;
		};
	}
	, containsFilterForFragment: function(fields, fragment){
		var filters = [];
		for(var index in fields){
			var field = fields[index];
			var filter = Utils.containsFilter(field, fragment);
			filters.push(filter);
		}
		return Utils.filterOr(filters);
	}
	, createFilterFragments: function(filter){
		var fragments = filter.split(' ');
		var results = [];
		for(var index in fragments){
			var fragment = fragments[index].trim();
			if(fragment.length > 0){
				results.push(fragment);
			}
		}
		return results;
	}
	, filterList: function(list, filter){
		var results = [];
		for(var index in list){
			var item = list[index];
			if(filter(item)){
				results.push(item);
			}
		}
		return results;
	}
};
