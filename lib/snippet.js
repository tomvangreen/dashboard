Snippets = new Mongo.Collection('snippets');

Snippet = new SimpleSchema({
	title: {
		type: String
		, label: "Title"
		, max: 200
	}
	, content: {
		type: String
		, label: "Content"
	}
	, description: {
		type: String
		, label: "Description"
		, optional: true
	}
	, language: {
		type: String
		, label: "Language"
		, max: 24
		, optional: true
	}
	, rating: {
		type: Number
		, label: "Rating"
		, optional: true
	}	
	, tags: {
		type: [String]
		, label: "Tags"
		, optional: true
	}
});
Snippets.attachSchema(Snippet);

SnippetService = {
	list: function(fragments){
		var filters = [];
		for(var index in fragments){
			var fragment = fragments[index];
			var filter = Utils.containsFilterForFragment(['title', 'content', 'description', 'language', 'tags'], fragment);
			filters.push(filter);
		}
		var chain = Utils.filterAnd(filters);
		var all = Snippets.find().fetch();
		return Utils.filterList(all, chain, true);
	}
	, rate: function(id, rating, toggle){
		var snippet = Snippets.findOne(id);
		var endRating = snippet.rating;
		if(toggle){
			if(endRating == rating){
				endRating = null;
			} else{
				endRating = rating;
			}
		}
		else{
			endRating = rating;
		}
		Snippets.update(snippet._id, {$set:{ rating: endRating}});
	}
	, edit: function(id){
		var snippet = Snippets.findOne(id);
		Modal.show('editSnippet', snippet);
	}};

