DashboardService = {
	DATA_KEY: 'dashboard'
	, ensureData: function(){
		var data = Session.get(DashboardService.DATA_KEY);
		if(!data){
			data = {
				sort: "title"
				, sortReverese: false
				, filter: ""
				, fragments: []
			};
			Session.set(DashboardService.DATA_KEY, data);
		}
		return data;
	}
	, changeFilter: function(filter){
		var data = DashboardService.ensureData();
		if(data.filter != filter){
			data.filter = filter;
			data.fragments = Utils.createFilterFragments(filter.toLowerCase());
			Session.set(DashboardService.DATA_KEY, data);		
		}
	}
	, changeSort: function(field){
		var data = DashboardService.ensureData();
		if(data.sort == field){
			data.sortReverese = !data.sortReverese;
		}else{
			data.sort = field;
			data.sortReverese = false;
		}
		Session.set(DashboardService.DATA_KEY, data);
	}
	, list: function(){
		var results = [];
		var data = DashboardService.ensureData();
		var links = LinkService.list(data.fragments);
		for(var index in links){
			var link = links[index];
			results.push({
				_id: link._id
				, title: link.title
				, content: link.url
				, description: link.description
				, type: 'Link'
				, titleUrl: link.url
				, titleTarget: "/links/" + link._id
				, contentUrl: link.url
				, contentTarget: "_blank"
				, tags: Utils.loadTags(link)
				, template: 'dashboardLink'
				, rating: link.rating ? link.rating : 0
			});
		}
		var snippets = SnippetService.list(data.fragments);
		for(var index in snippets){
			var snippet = snippets[index];
			var title = snippet.title;
			if(snippet.language){
				title = title + " (" + snippet.language + ")";
			}
 			results.push({
				_id: snippet._id
				, title: title
				, content: snippet.content
				, description: snippet.description
				, type: 'Code'
				, titleUrl: '/snippets/' + snippet._id
				, titleTarget: ''
				, contentUrl: null
				, contentTarget: ''
				, tags: Utils.loadTags(snippet)
				, template: 'dashboardSnippet'
				, expanded: false
				, rating: snippet.rating ? snippet.rating : 0
			});
		}
		return Utils.sort(results, data.sort, data.sortReverese);
	}
	, editItem: function(id, type){
		if(type == "Link"){
			LinkService.edit(id);
		}else if(type == "Code"){
			SnippetService.edit(id);
		}
	}
	, removeItem: function(id, type){
		//TODO: Display which item it is
		if(confirm("Please confirm the deletion")){
			if(type == 'Link'){
				Links.remove(id);
			} else if(type == 'Code'){
				Snippets.remove(id);
			}
		}
	}
	, rate: function(id, type, rating){
		if(type == "Link") {
			LinkService.rate(id, rating, true);
		} else if(type == "Code"){
			SnippetService.rate(id, rating, true);
		}
	}

};


