Links = new Mongo.Collection('links');

Link = new SimpleSchema({
	title: {
		type: String
		, label: "Title"
		, max: 200
	}
	, url: {
		type: String
		, label: "Url"
	}
	, description: {
		type: String
		, label: "Description"
		, optional: true
	}
	, rating: {
		type: Number
		, label: "Rating"
		, optional: true
	}
	, tags: {
		type: [String]
		, label: "Tags"
		, optional: true
	}
});
Links.attachSchema(Link);

LinkService = {
	list: function(fragments){
		var filters = [];
		for(var index in fragments){
			var fragment = fragments[index];
			var filter = Utils.containsFilterForFragment(['title', 'url', 'description', 'tags'], fragment);
			filters.push(filter);
		}
		var chain = Utils.filterAnd(filters);
		var all = Links.find().fetch();
		return Utils.filterList(all, chain);
	}
	, rate: function(id, rating, toggle){
		var link = Links.findOne(id);
		var endRating = link.rating;
		if(toggle){
			if(endRating == rating){
				endRating = null;
			} else{
				endRating = rating;
			}
		}
		else{
			endRating = rating;
		}
		Links.update(link._id, {$set:{ rating: endRating}});
	}
	, edit: function(id){
		var link = Links.findOne(id);
		Modal.show('editLink', link);
	}
};

