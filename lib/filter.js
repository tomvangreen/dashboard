Filters = new Mongo.Collection('filters');

Filter = new SimpleSchema({
	title: {
		type: String
		, label: 'Title'
	}
	, fragments: {
		type: [String]
		, label: 'Fragments'
	}
	, categories: {
		type: [String]
		, label: 'Categories'
	}
	, labels: {
		type: [String]
		, label: 'Labels'
	}
});
Filters.attachSchema(Filter);
