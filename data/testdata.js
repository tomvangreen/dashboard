if(Meteor.isServer){
	loadTestData = function(){
	    var empty = function(collection){
	      collection.remove({});
	    };
	    var insertAll = function(collection, list){
	      for(var index in list){
	        var item = list[index];
	        var found = collection.find({title: item.title}).fetch();
	        if(!found || found.length == 0){
	          collection.insert(item);
	        }
	      }
	    };

	    empty(Links);
	    empty(Snippets);

	    var links = [
	      {title: 'Reactivity Basics: Meteor\'s Magic Demystified', url: 'https://www.discovermeteor.com/blog/reactivity-basics-meteors-magic-demystified/', description: 'Tutorial about some meteor related things...', tags: ['meteor'], rating: 3}
	      , {title: 'JGO', url: 'http://www.java-gaming.org/', tags: ['gamedev', 'java'] }
	      , {title: 'Atombrot - Homepage', url: 'http://www.atombrot.com/', tags: ['personal', 'sound'] }
	      , {title: 'Atombrot - Soundcloud', url: 'http://www.soundcloud.com/atombrot', tags: ['personal', 'sound']  }
	      , {title: 'Mentex - Page', url: 'http://mentex.ch/', tags: ['personal', 'dev', 'sound']}
	      , {title: 'Mentex - Soundcloud', url: 'https://soundcloud.com/mentalextract', tags: ['personal','sound']}
	      , {title: 'Github', rating: 3, url: 'http://www.github.com', tags: ['dev'] }
	      , {title: 'Ludum Dare', url: 'http://www.ludumdare.com', tags: ['gamedev', 'jams'] }
	      , {title: 'Thewall', url: 'http://www.thewall.de', tags: ['gamedev'], description: 'German Half-Life/Half-Life 2 mapping and modding page. Page itself is not very active, but there is some activity in the forum.' }
	      , {title: 'Gamasutra', rating: 5, url: 'http://www.gamasutra.com', tags: ['gamedev'] }
	      , {title: 'SBB', url: 'http://www.sbb.ch', tags: ['travel'] }
	      , {title: 'Digitec', url: 'http://www.digitec.ch', tags: ['tech', 'store'] }
	      , {title: 'Golem.de', rating: 1, url: 'http://www.golem.de', tags: ['news'] }
	      , {title: 'Heise', url: 'http://www.heise.de', tags: ['news'] }
	      , {title: 'Meteor', rating: 4, url: 'https://www.meteor.com/', tags: ['dev', 'javascript', 'meteor']}
	      , {title: 'Netflix', url: 'http://www.netflix.com', tags: ['tv'] }
	      , {title: 'Wikipedia', url: 'http://www.wikipedia.org', tags: ['knowledge']}
	      , {title: 'Youtube', url: 'http://www.youtube.com', tags: ['tv'] }
	      , {title: 'TV Tropes', url: 'http://www.tvtropes.org', tags: ['tv', 'story'] }
	      , {title: 'Twitch', url: 'http://www.twitch.tv', tags: ['tv'] }
	      , {title: 'Amazon', url: 'http://www.amazon.com', tags: ['store'] }
	      , {title: 'Think Geek', url: 'http://www.thinkgeek.com', tags: ['store'] }
	      , {title: 'Virtual Arsenal Closing Event Reference', url: 'http://forums.bistudio.com/showthread.php?189495-End-Game-MP-Mode-Feedback/page12', tags: ['arma'] }
	      , {title: 'iniDB text file db', url: 'http://www.armaholic.com/page.php?id=23340', tags: ['arma'] }
	      , {title: 'Stackoverflow', url: 'http://www.stackoverflow.com', tags: ['dev']}
	      , {title: 'Stackexchange', url: 'http://www.stackexchange.com', tags: ['dev']}
	      , {title: 'Programmers Stackexchange', url: 'http://programmers.stackexchange.com', tags: ['dev']}
	      , {title: 'Workplace Stackexchange', url: 'http://workplace.stackexchange.com', tags: ['work']}
	      , {title: 'Gamedev Stackexchange', url: 'http://gamedev.stackexchange.com', tags: ['dev', 'gamedev']}
	      , {title: 'RPG Stackexchange', url: 'http://rpg.stackexchange.com', tags: ['rpg', 'story', 'games']}
	      , {title: 'Scifi Stackexchange', url: 'http://scifi.stackexchange.com', tags: ['story', 'scifi']}
	      , {title: 'Libgdx Pinch To Zoom Tutorial', url: 'http://www.steventrigg.com/libgdx-pinch-to-zoom/', tags: ['tutorial']}
	      , {title: 'Didgeridrone Didge Generator', url: 'http://mynoise.net/NoiseMachines/didgeridooDroneGenerator.php', tags: ['sound', 'tools']}
	      , {title: 'Killzone Kids Arma Tutorial', url: 'http://killzonekid.com/arma-scripting-tutorials-triggers-v2/', tags: ['arma']}
	      , {title: 'Shadertoy', url: 'https://www.shadertoy.com/', tags: ['dev', 'gamedev', 'shaders']}
	      , {title: 'Shadertoy Galaxy', url: 'https://www.shadertoy.com/view/MslGWN', tags: ['dev', 'gamedev', 'shaders']}
	      , {title: 'Arma GID Object Positioning System Tutorial', url: 'https://www.youtube.com/watch?v=_Qq3YafnwhU', tags: ['arma', 'tutorial']}
	      , {title: 'Arma 3 Respoawn', url: 'https://community.bistudio.com/wiki/Arma_3_Respawn', tags: ['arma']}
	      , {title: 'Bootstrap Tag Inputs', url: 'http://timschlechter.github.io/bootstrap-tagsinput/examples/', tags: ['dev', 'libs', 'meteor', 'bootstrap']}
	      , {title: 'Meteoer Bootstrap Modal', url: 'https://github.com/PeppeL-G/bootstrap-3-modal/', tags: ['dev', 'libs', 'meteor', 'bootstrap']}
	      , {title: 'Meteor and Bootstrap the right way', url: 'http://www.manuel-schoebel.com/blog/meteorjs-and-twitter-bootstrap---the-right-way', tags: ['dev', 'meteor', 'libs', 'bootstrap']}
	    ];
	    var snippets = [
	      {title: 'Hello World', rating: 3, content: 'console.log("Hello World");\nalert("Hello World");\nconfirm("Hello World");', language: 'javascript', tags: ["javascript"]}
	      , {title: 'Function Call', rating: 5, content: '[] call fn_cc;', language: 'sqf', tags: ["arma"], description: 'Basic function call of an already compiled function.'}
	      , {title: 'Groovy Collection Stuff', content: 'list\n  .findAll{ it.complies() }\n  .toSet()', language: 'groovy', tags: ["groovy"]}
	      , {title: 'Some Code', content: 'content', tags: ['arma']}
	    ];
	    insertAll(Links, links);
	    insertAll(Snippets, snippets);

	}
}