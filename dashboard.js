if (Meteor.isClient) {
  // counter starts at 0
  Session.setDefault('counter', 0);

  Template.hello.helpers({
    counter: function () {
      return Session.get('counter'); 
    }
  });

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    }
  });
  Template.dashboardBox.helpers({
    links: function(){
      return DashboardService.list('');
    }
  });
  Template.dashboardBox.events({
    'click th.sortable': function(event, template){
      var target = event.target;
      var sort = target ? $(target).attr('sort') : null;
      if(sort){
        DashboardService.changeSort(sort);
      }
    }
    , 'keyup input.filter': function(event, template){
      DashboardService.changeFilter(event.target.value);
    }
    , 'click button.edit': function(event, template){
      var element = $(event.target).parents(".item");
      var itemId = element.attr('itemid');
      var type = element.attr('type');
      DashboardService.editItem(itemId, type);
    }
    , 'click button.remove': function(event, template){
      var element = $(event.target).parents(".item");
      var itemId = element.attr('itemid');
      var type = element.attr('type');
      DashboardService.removeItem(itemId, type);
    }
  });
  Template.dashboardBox.rendered = function()
  {
      this.$('input').focus();
  };
  Template.dashboardSnippet.helpers({
  });
  Template.dashboardSnippet.events({
    'click button.toggle': function(event, template){
      var next = $(event.target).parents(".buttons").next();
      console.log(next);
      next.toggle(200);
    }
  });
  Template.rating.rendered = function (){
    var rating = this.data.rating;
    if(!rating){
      rating = 0;
    }
    for(var index = 0; index < 5; index++){
      if(index < rating){
        this.$('.star-' + (index + 1)).addClass("glyphicon-star")
        this.$('.star-' + (index + 1)).removeClass("glyphicon-star-empty")
      } else{
        this.$('.star-' + (index + 1)).addClass("glyphicon-star-empty")
        this.$('.star-' + (index + 1)).removeClass("glyphicon-star")        
      }
    }
  };
  Template.rating.events({
    'click .star': function(event, template){
      var element = $(event.target);
      if(!element.hasClass("star")){
        var element = element.parents(".star");
      }
      var row = element.parents(".item");
      var itemId = row.attr('itemid');
      var type = row.attr('type');
      var rating = 0;
      for(var index = 1; index <= 5; index++){
        var className = "star-" + index;
        if(element.hasClass(className)){
          rating = index;
        }
      }
      DashboardService.rate(itemId, type, rating);
    }
  });
  Template.editSnippet.rendered = function(event, template){
    var editor = CodeMirror.fromTextArea(this.find("#code"), {
        lineNumbers: true,
        mode: "javascript" // set any of supported language modes here
    });
  };
}
if (Meteor.isServer) {
  Meteor.startup(function () {
    loadTestData();
  });
}

Router.route('/', function(){
  this.render('home', DashboardService.ensureData());
});
Router.route('/links', function(){
  this.render('links');
});
Router.route('/snippets/:_id', function(){
  var snippet = Snippets.findOne({ _id: this.params._id});
  this.render('/snippet', snippet);
});
Router.route('/snippets', function(){
  this.render('snippets');
});
Router.route('/tags', function(){
  this.render('tags');
});
Router.route('/tags/:tag', function(){
  this.render('tag');
});

